package com.example.fardin.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0 , 100 , 0 ,"contact to us");
        menu.add(0 , 200 , 2 ,"Email me");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==100)
            Toast.makeText(this, "contact to us", Toast.LENGTH_SHORT).show();

        if (item.getItemId()==200)
            Toast.makeText(this, "Email me", Toast.LENGTH_SHORT).show();

        if (item.getItemId()==R.id.setting)
            Toast.makeText(this, "setting", Toast.LENGTH_SHORT).show();

        if (item.getItemId()==R.id.signup)
            Toast.makeText(this, "signup", Toast.LENGTH_SHORT).show();

        return super.onOptionsItemSelected(item);
    }
}
