package com.example.fardin.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.fardin.myapplication.R;

/**
 * Created by fardin on 15/06/2017.
 */
class studentListAdapter extends BaseAdapter {
    Context mContext;
    String name[];

    public studentListAdapter(String[] name, Context mContext) {
        this.name = name;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return name[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate( R.layout.drawer_item, viewGroup, false);
        return null;
    }
}
